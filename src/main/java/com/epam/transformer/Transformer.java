package com.epam.transformer;


import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKeyComposite;
import com.epam.model.annotation.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private static final Logger logger = LogManager.getLogger(Transformer.class.getName());
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet rs) {
        Object entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field f : fields) {
                    if (f.isAnnotationPresent(Column.class)) {
                        setValueInField(f, rs, entity);
                    } else if (f.isAnnotationPresent(PrimaryKeyComposite.class)) {
                        Class fieldType = f.getType();
                        f.setAccessible(true);
                        Object ForeignKey = fieldType.getConstructor().newInstance();
                        f.set(entity, ForeignKey);
                        f.setAccessible(false);
                        Field[] innerFields = fieldType.getDeclaredFields();
                        for (Field innerField : innerFields) {
                            if (innerField.isAnnotationPresent(Column.class)) {
                                setValueInField(innerField, rs, ForeignKey);
                            }
                        }
                    }
                }
            }
        } catch (SQLException e){
            logger.error("ErrorCode = " + e.getErrorCode()
                    +", SQLState = " + e.getSQLState()
                    + ", " + e.getLocalizedMessage());
        }  catch (Exception e) {
            logger.error("Transformer error: " + e.getLocalizedMessage());
        }
        return entity;
    }

    private void setValueInField(Field field, ResultSet resultSet,
                                 Object entity) throws SQLException, IllegalAccessException {
        field.setAccessible(true);
        Column column = field.getAnnotation(Column.class);
        String name = column.name();
        Class fieldType = field.getType();
        if (fieldType == String.class) field.set(entity,
                resultSet.getString(name));
        else if (fieldType == int.class) field.set(entity,
                resultSet.getInt(name));
        else if (fieldType == Date.class) field.set(entity,
                resultSet.getDate(name));
        else if (fieldType == boolean.class) field.set(entity,
                resultSet.getBoolean(name));
        field.setAccessible(false);
    }
}
