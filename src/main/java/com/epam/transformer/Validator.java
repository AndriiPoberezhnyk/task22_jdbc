package com.epam.transformer;

import com.epam.model.annotation.Column;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

public class Validator {
    private static final Logger logger = LogManager.getLogger(Validator.class.getName());

    public static <T> boolean validate(T t) {
        if (!checkStringLength(t)) {
            logger.error("Validation failed");
            return false;
        } else {
            logger.info("Validation success");
            return true;
        }
    }

    private static boolean checkStringLength(Object entity) {
        Field[] fields = entity.getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class)) {
                    field.setAccessible(true);
                    Class targetType = field.getType();
                    int length = field.getAnnotation(Column.class).length();
                    if (targetType == String.class) {
                        String value = (String) field.get(entity);
                        if (value.length() > length) {
                            return false;
                        }
                    }
                    field.setAccessible(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Check entity error: " + e.getLocalizedMessage() + e.getMessage());
        }
        logger.info("Check for string length success");
        return true;
    }
}
