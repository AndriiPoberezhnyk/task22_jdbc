package com.epam.DAO.locationDAO;

import com.epam.DAO.GeneralDAO;
import com.epam.model.location.CityEntity;

import java.sql.SQLException;

public interface CityDAO extends GeneralDAO<CityEntity, String> {
    int update(CityEntity oldEntity, CityEntity newEntity) throws SQLException;
}
