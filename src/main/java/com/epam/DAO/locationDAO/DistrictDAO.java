package com.epam.DAO.locationDAO;

import com.epam.DAO.GeneralDAO;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.PK_District;

import java.sql.SQLException;
import java.util.List;

public interface DistrictDAO extends GeneralDAO<DistrictEntity, PK_District> {
    List<DistrictEntity> findByDistrict(String name) throws SQLException;

    List<DistrictEntity> findByCity(String cityName) throws SQLException;
}
