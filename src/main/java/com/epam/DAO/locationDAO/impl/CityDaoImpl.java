package com.epam.DAO.locationDAO.impl;

import com.epam.DAO.locationDAO.CityDAO;
import com.epam.model.location.CityEntity;
import com.epam.persistant.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CityDaoImpl implements CityDAO {
    private static final String FIND_ALL = "SELECT * FROM city";
    private static final String DELETE = "DELETE FROM city WHERE city_name=?";
    private static final String CREATE = "INSERT city (city_name) VALUES (?)";
    private static final String UPDATE = "UPDATE city SET city_name=? WHERE city_name=?";
    private static final String FIND_BY_ID = "SELECT * FROM city WHERE city_name=?";

    @Override
    public List<CityEntity> findAll() throws SQLException {
        List<CityEntity> cities = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    cities.add((CityEntity)new Transformer(CityEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return cities;
    }

    @Override
    public CityEntity findById(String cityName) throws SQLException {
        CityEntity entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, cityName);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(CityEntity)new Transformer(CityEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(CityEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setString(1,entity.getCityName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(CityEntity entity) throws SQLException {
        return 0;
    }

    @Override
    public int update(CityEntity oldEntity, CityEntity newEntity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1, newEntity.getCityName());
            ps.setString(2, oldEntity.getCityName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setString(1,id);
            return ps.executeUpdate();
        }
    }
}
