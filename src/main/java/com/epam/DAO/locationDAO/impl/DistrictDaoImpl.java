package com.epam.DAO.locationDAO.impl;

import com.epam.DAO.locationDAO.DistrictDAO;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.PK_District;
import com.epam.persistant.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DistrictDaoImpl implements DistrictDAO {
    private static final String FIND_ALL = "SELECT * FROM district";
    private static final String DELETE = "DELETE FROM city WHERE city_name=?";
    private static final String CREATE = "INSERT district (district_name, city_name) VALUES (?,?)";
    private static final String UPDATE = "UPDATE city SET city_name=? WHERE city_name=?";
    private static final String FIND_BY_ID = "SELECT * FROM city WHERE city_name=?";

    @Override
    public List<DistrictEntity> findByDistrict(String districtName) throws SQLException {
        List<DistrictEntity> districts = new ArrayList<>();
        StringBuilder query = new StringBuilder(FIND_ALL);
        query.append(" WHERE district_name =?");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setString(1, districtName);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    districts.add((DistrictEntity)new Transformer(DistrictEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return districts;
    }

    @Override
    public List<DistrictEntity> findByCity(String cityName) throws SQLException {
        List<DistrictEntity> districts = new ArrayList<>();
        StringBuilder query = new StringBuilder(FIND_ALL);
        query.append(" WHERE city_name =?");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setString(1, cityName);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    districts.add((DistrictEntity)new Transformer(DistrictEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return districts;
    }

    @Override
    public List<DistrictEntity> findAll() throws SQLException {
        List<DistrictEntity> districts = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    districts.add((DistrictEntity)new Transformer(DistrictEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return districts;
    }

    @Override
    public DistrictEntity findById(PK_District pk_district) throws SQLException {
        return null;
    }

    @Override
    public int create(DistrictEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setString(1,entity.getDistrictName());
            ps.setString(2,entity.getCityName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(DistrictEntity entity) throws SQLException {
        return 0;
    }

    @Override
    public int delete(PK_District pk_district) throws SQLException {
        return 0;
    }
}
