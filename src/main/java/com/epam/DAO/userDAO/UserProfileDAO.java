package com.epam.DAO.userDAO;

import com.epam.DAO.GeneralDAO;
import com.epam.model.user.UserEntity;

import java.sql.SQLException;
import java.util.List;

public interface UserProfileDAO extends GeneralDAO<UserEntity, Integer> {
    List<UserEntity> findByNickname(String name) throws SQLException;

    List<UserEntity> findByMail(String name) throws SQLException;
}
