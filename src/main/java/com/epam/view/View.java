package com.epam.view;

import com.epam.controller.DistrictController;
import com.epam.controller.GeneralController;
import com.epam.controller.impl.CityControllerImpl;

import com.epam.controller.impl.DistrictControllerImpl;
import com.epam.persistant.ConnectionManager;
import com.epam.view.table_views.DistrictTable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);
    private ResourceBundle bundle;


    public View() {
        bundle = ResourceBundle.getBundle("Menu");
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        ConnectionManager.setConnection();
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::changeCityTable);
        methodsMenu.put("2", this::changeDistrictTable);

        outputMenu();
    }

    private void changeCityTable() {
        GeneralController cityController = new CityControllerImpl();
        BasicTableView cityView = new BasicTableView(cityController);
        cityView.run();
    }

    private void changeDistrictTable() {
        DistrictController districtController = new DistrictControllerImpl();
        BasicTableView districtView = new DistrictTable(districtController);
        districtView.run();
    }

    protected void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
