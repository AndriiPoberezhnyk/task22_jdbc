package com.epam.model.location;

import com.epam.model.annotation.PrimaryKeyComposite;
import com.epam.model.annotation.Table;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Table(name = "district")
@Setter
@Getter
public class DistrictEntity {
    private static final Logger logger = LogManager.getLogger(DistrictEntity.class.getName());
    @PrimaryKeyComposite
    private PK_District pkDistrict;

    public DistrictEntity() {
    }

    public DistrictEntity(PK_District pkDistrict) {
        this.pkDistrict = pkDistrict;
    }

    public String getCityName() {
        return pkDistrict.getCityName();
    }

    public String getDistrictName() {
        return pkDistrict.getDistrictName();
    }

    @Override
    public String toString() {
        return pkDistrict.toString();
    }
}
