package com.epam.model.location;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;
import lombok.Getter;
import lombok.Setter;

@Table(name = "city")
@Getter
@Setter
public class CityEntity {
    @Column(name = "city_name", length = 10)
    private String cityName;

    public CityEntity() {
    }

    public CityEntity(String city) {
        this.cityName = city;
    }

    @Override
    public String toString() {
        return "City{" +
                "cityName='" + cityName + '\'' +
                '}';
    }

}
