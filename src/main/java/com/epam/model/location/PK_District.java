package com.epam.model.location;

import com.epam.model.annotation.Column;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PK_District {
    @Column(name = "district_name", length = 45)
    private String districtName;
    @Column(name = "city_name", length = 45)
    private String cityName;

    public PK_District() {
    }

    public PK_District(String districtName, String cityName) {
        this.districtName = districtName;
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return "District{" +
                "districtName='" + districtName + '\'' +
                ", cityName='" + cityName + '\'' +
                '}';
    }
}
