package com.epam.model.user;

import com.epam.model.annotation.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PK_Payment {
    @Column(name = "payment_method", length= 45)
    private String payMethod;
    @Column(name = "payment_time", length= 45)
    private String payTime;

    public PK_Payment() {
    }

    public PK_Payment(String payMethod, String payTime) {
        this.payMethod = payMethod;
        this.payTime = payTime;
    }

    @Override
    public String toString() {
        return "UserPayment{" +
                "payMethod='" + payMethod + '\'' +
                ", payTime='" + payTime + '\'' +
                '}';
    }
}
