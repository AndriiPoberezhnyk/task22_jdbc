package com.epam.model.user;

import com.epam.model.annotation.PrimaryKeyComposite;
import com.epam.model.annotation.Table;
import lombok.Getter;
import lombok.Setter;

@Table(name = "payment")
@Setter
public class UserPaymentEntity {
    @PrimaryKeyComposite
    private PK_Payment pkPayment;

    public UserPaymentEntity() {
    }

    public UserPaymentEntity(PK_Payment pkPayment) {
        this.pkPayment = pkPayment;
    }

    public String getPaymentMethod(){
        return pkPayment.getPayMethod();
    }

    public String getPaymentTime(){
        return pkPayment.getPayTime();
    }

    @Override
    public String toString() {
        return pkPayment.toString();
    }
}
