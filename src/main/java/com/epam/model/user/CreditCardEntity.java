package com.epam.model.user;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Table(name = "credit_cart")
@Getter
@Setter
public class CreditCardEntity {
    @PrimaryKey
    @Column(name = "user_profile_id")
    private int id;
    @Column(name = "credit_cart" , length = 45)
    private String creditCart;
    @Column(name = "owner_name" , length = 45)
    private String ownerName;
    @Column(name = "expiration")
    private Date expDate;
    @Column(name = "rewards" , length = 1)
    private boolean rewards;
    @Column(name = "business_trip" , length = 1)
    private boolean useForBusinessTrip;

    public CreditCardEntity() {
    }

    public CreditCardEntity(int id, String creditCart, String ownerName, Date expDate, boolean rewards, boolean useForBusinessTrip) {
        this.id = id;
        this.creditCart = creditCart;
        this.ownerName = ownerName;
        this.expDate = expDate;
        this.rewards = rewards;
        this.useForBusinessTrip = useForBusinessTrip;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "id=" + id +
                ", creditCart='" + creditCart + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", expDate=" + expDate +
                ", rewards=" + rewards +
                ", useForBusinessTrip=" + useForBusinessTrip +
                '}';
    }
}
