package com.epam.model.user;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;
import lombok.Getter;
import lombok.Setter;

@Table(name = "user_security")
@Getter
@Setter
public class UserSecurityEntity {
    @Column(name = "user_profile_id")
    private int id;
    @Column(name = "password")
    private String password;

    public UserSecurityEntity() {
    }

    public UserSecurityEntity(int id, String password) {
        this.id = id;
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserSecurityEntity{" +
                "id=" + id +
                ", password='" + password + '\'' +
                '}';
    }
}
