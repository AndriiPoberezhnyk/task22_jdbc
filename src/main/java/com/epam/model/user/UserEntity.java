package com.epam.model.user;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Table(name = "user_profile")
@Getter
@Setter
public class UserEntity {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "nickname", length= 45)
    private String nickname;
    @Column(name = "birth_day")
    private Date birthday;
    @Column(name = "country", length= 45)
    private String country;
    @Column(name = "email", length= 45)
    private String email;
    @Column(name = "facebook_link")
    private String facebookLink;
    @Column(name = "google_link")
    private String googleLink;
    @Column(name = "appeal", length= 45)
    private String appeal;
    @Column(name = "name", length= 45)
    private String name;
    @Column(name = "surname", length= 45)
    private String surname;
    @Column(name = "phone", length= 15)
    private String phone;
    @Column(name = "address", length= 45)
    private String address;
    private UserPaymentEntity payment;
    @Column(name = "smoking", length= 1)
    private boolean smoking;
    @Column(name = "hotel_stars", length= 1)
    private int hotelStars;
    @Column(name = "disabled", length= 1)
    private boolean disabled;
    @Column(name = "booking_for", length= 45)
    private String bookingFor;
    @Column(name = "currency", length= 45)
    private String currency;

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", birthday='" + birthday + '\'' +
                ", country='" + country + '\'' +
                ", email='" + email + '\'' +
                ", facebookLink='" + facebookLink + '\'' +
                ", googleLink='" + googleLink + '\'' +
                ", appeal='" + appeal + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", payMethod='" + payment.getPaymentMethod() + '\'' +
                ", payTime='" + payment.getPaymentTime() + '\'' +
                ", smoking=" + smoking +
                ", hotelStars='" + hotelStars + '\'' +
                ", disabled=" + disabled +
                ", bookingFor='" + bookingFor + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
