package com.epam.persistant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class ConnectionManager {
    private static final Logger logger =
            LogManager.getLogger(ConnectionManager.class.getName());
    private static String url;
    private static String driverName;
    private static String user;
    private static String password;

    private static Connection connection = null;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
            }
        }
        return connection;
    }

    public static void setConnection() {
        FileInputStream fis;
        try {
            Properties p = new Properties();
            fis = new FileInputStream("src/main/resources/connection" +
                    ".properties");
            p.load(fis);
            driverName = (String) p.get("driver");
            url = (String) p.get("URL");
            user = (String) p.get("username");
            password = (String) p.get("password");
        } catch (FileNotFoundException e) {
            logger.error("Error while reading .properties: " + e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error("Error while load properties: " + e.getLocalizedMessage());
        }
    }
}
