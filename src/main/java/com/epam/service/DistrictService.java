package com.epam.service;

import com.epam.DAO.locationDAO.impl.DistrictDaoImpl;
import com.epam.model.location.DistrictEntity;

import java.sql.SQLException;
import java.util.List;

public class DistrictService {
    public List<DistrictEntity> findAll() throws SQLException {
        return new DistrictDaoImpl().findAll();
    }

    public int create(DistrictEntity entity) throws SQLException {
        return new DistrictDaoImpl().create(entity);
    }

    public List<DistrictEntity> findByCity(String cityName) throws SQLException {
        return new DistrictDaoImpl().findByCity(cityName);
    }

    public List<DistrictEntity> findByDistrict(String districtName) throws SQLException {
        return new DistrictDaoImpl().findByDistrict(districtName);
    }
}
