package com.epam.service;

import com.epam.DAO.locationDAO.impl.CityDaoImpl;
import com.epam.model.location.CityEntity;

import java.sql.SQLException;
import java.util.List;

public class CityService {
    public List<CityEntity> findAll() throws SQLException {
        return new CityDaoImpl().findAll();
    }

    public CityEntity findById(String id) throws SQLException {
        return new CityDaoImpl().findById(id);
    }

    public int create(CityEntity entity) throws SQLException {
        return new CityDaoImpl().create(entity);
    }

    public int update(CityEntity oldEntity, CityEntity newEntity) throws SQLException {
        return new CityDaoImpl().update(oldEntity, newEntity);
    }

    public int delete(String city_name) throws SQLException {
        return new CityDaoImpl().delete(city_name);
    }
}
