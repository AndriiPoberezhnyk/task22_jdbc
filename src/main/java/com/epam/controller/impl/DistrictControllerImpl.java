package com.epam.controller.impl;

import com.epam.controller.DistrictController;
import com.epam.model.location.DistrictEntity;
import com.epam.model.location.PK_District;
import com.epam.service.DistrictService;
import com.epam.transformer.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class DistrictControllerImpl implements DistrictController {
    private static final Logger logger = LogManager.getLogger(DistrictControllerImpl.class.getName());
    private static Scanner scanner = new Scanner(System.in);
    DistrictService districtService = new DistrictService();

    @Override
    public void select() throws SQLException {
        System.out.println("Table: District");
        List<DistrictEntity> districts = districtService.findAll();
        for (DistrictEntity entity : districts) {
            logger.info(entity);
        }
    }

    @Override
    public void create() throws SQLException {
        DistrictEntity districtEntity= createDistrictEntity();
        int count = districtService.create(districtEntity);
        logger.info("There are created "+count+" rows");
    }

    @Override
    public void delete() throws SQLException {

    }

    @Override
    public void update() throws SQLException {

    }

    @Override
    public void findByID() throws SQLException {

    }

    @Override
    public void findByCity() throws SQLException {
        logger.info("Input city_name to select from District: ");
        String cityName = scanner.nextLine();
        for (DistrictEntity entity : districtService.findByCity(cityName)) {
            logger.info(entity);
        }
    }

    @Override
    public void findByDistrict() throws SQLException {
        logger.info("Input district_name to select from District: ");
        String districtName = scanner.nextLine();
        for (DistrictEntity entity : districtService.findByDistrict(districtName)) {
            logger.info(entity);
        }
    }

    @Override
    public String getTableName() {
        return "District";
    }

    private DistrictEntity createDistrictEntity() throws SQLException {
        DistrictEntity districtEntity = new DistrictEntity();
        logger.info("Input city_name for District: ");
        String cityName = scanner.nextLine();
        logger.info("Input district_name for District: ");
        String districtName = scanner.nextLine();
        districtEntity.setPkDistrict(new PK_District(districtName,cityName));
        if(Validator.validate(districtEntity)){
            return districtEntity;
        }else{
            throw new SQLException("Wront input values");
        }
    }
}
