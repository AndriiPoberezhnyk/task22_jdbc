package com.epam.controller.impl;

import com.epam.controller.CityController;
import com.epam.model.location.CityEntity;
import com.epam.service.CityService;
import com.epam.transformer.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class CityControllerImpl implements CityController {
    private static final Logger logger = LogManager.getLogger(CityControllerImpl.class.getName());
    private static Scanner scanner = new Scanner(System.in);

    @Override
    public void delete() throws SQLException {
        logger.info("Input city_name for City ");
        String cityName = scanner.nextLine();
        CityService cityService = new CityService();
        int count = cityService.delete(cityName);
        logger.info("There are deleted "+count+" rows");
    }

    @Override
    public void create() throws SQLException {
        CityService cityService = new CityService();
        int count = cityService.create(createCityEntity());
        logger.info("There are created "+count+" rows");
    }

    @Override
    public void update() throws SQLException {
        logger.info("Input old city_name for City: ");
        String oldCityName = scanner.nextLine();
        CityEntity oldEntity = new CityEntity(oldCityName);
        logger.info("Input new city_name for City: ");
        String newCityName = scanner.nextLine();
        CityEntity newEntity = new CityEntity(newCityName);
        CityService cityService = new CityService();
        int count = cityService.update(oldEntity, newEntity);
        logger.info("There are updated "+count+" rows");
    }

    @Override
    public void select() throws SQLException {
        logger.info("Table: City");
        CityService cityService = new CityService();
        List<CityEntity> cities = cityService.findAll();
        for (CityEntity entity : cities) {
            logger.info(entity);
        }
    }

    @Override
    public void findByID() throws SQLException {

    }

    @Override
    public String getTableName() {
        return "City";
    }

    private CityEntity createCityEntity() throws SQLException {
        logger.info("Input city_name for City: ");
        String cityName = scanner.nextLine();
        CityEntity entity = new CityEntity(cityName);
        if(Validator.validate(entity)){
            return entity;
        }else{
            throw new SQLException("Wront input values");
        }
    }
}
