package com.epam.controller;

import java.sql.SQLException;

public interface GeneralController {
    void delete() throws SQLException;
    void create() throws SQLException;
    void update() throws SQLException;
    void select() throws SQLException;
    void findByID() throws SQLException;
    String getTableName();
}
