package com.epam.controller;

import java.sql.SQLException;

public interface DistrictController extends GeneralController {
    void findByCity() throws SQLException;
    void findByDistrict() throws SQLException;
}
